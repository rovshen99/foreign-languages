import nested_admin

from django.contrib import admin

from quiz.models import Quiz, Theme, Question, Answer, QuizSolution, QuizSolutionQuestion


class AnswerInline(nested_admin.NestedTabularInline):
    model = Answer
    extra = 0


class QuestionInline(nested_admin.NestedTabularInline):
    model = Question
    extra = 0
    inlines = [AnswerInline]
    sortable_by = ('sequence',)
    fields = ('id', 'text', 'score', 'sequence')
    readonly_fields = ('id',)


@admin.register(Quiz)
class QuizAdmin(nested_admin.NestedModelAdmin):
    list_display = ('id', 'name', 'theme', 'deadline')
    inlines = (QuestionInline,)
    readonly_fields = ('id',)


@admin.register(Theme)
class ThemeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name',)


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ('id', 'text', 'quiz', 'score', 'sequence')
    list_filter = ('quiz',)


@admin.register(Answer)
class AnswerAdmin(admin.ModelAdmin):
    list_display = ('id', 'text', 'question', 'correct', 'sequence')
    list_filter = ('question', 'question__quiz',)


@admin.register(QuizSolution)
class QuizSolutionAdmin(admin.ModelAdmin):
    list_display = ('id', 'quiz', 'user', 'point')
    readonly_fields = ('correct_count', 'wrong_count')
    list_filter = ('user',)


@admin.register(QuizSolutionQuestion)
class QuizSolutionQuestionAdmin(admin.ModelAdmin):
    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        parent_id = request.resolver_match.kwargs.get('object_id', None)
        if db_field.name == 'question':
            try:
                kwargs["queryset"] = Question.objects.filter(quiz__solutions__solution_questions=parent_id)
            except IndexError:
                pass
        if db_field.name == 'given_answer':
            try:
                question = QuizSolutionQuestion.objects.get(pk=parent_id).question
                kwargs["queryset"] = Answer.objects.filter(question=question)
            except Exception:
                pass
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    list_display = ('id', 'quiz_solution', 'question', 'given_answer')
