import datetime

from django.db import models
from django.contrib.auth import get_user_model


User = get_user_model()


class Quiz(models.Model):
    """"Тест"""
    name = models.TextField()
    theme = models.ForeignKey('Theme', on_delete=models.SET_NULL, related_name='quiz', null=True)
    deadline = models.DateTimeField(default=datetime.datetime.now() - datetime.timedelta(days=7))

    def __str__(self):
        return self.name


class Theme(models.Model):
    """Сфера для теста"""
    name = models.TextField()

    def __str__(self):
        return self.name


class Question(models.Model):
    """Вопрос для теста"""
    text = models.TextField()
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, related_name='questions')
    score = models.PositiveIntegerField(default=0,)
    sequence = models.PositiveIntegerField(verbose_name='Порядок вопроса в тесте', default=1)

    def __str__(self):
        return self.text

    def save(self, *args, **kwargs):
        if not self.pk:
            last_sequence = Question.objects.filter(quiz=self.quiz).aggregate(largest=models.Max('sequence'))['largest']
            if last_sequence is not None:
                self.sequence = last_sequence + 1
        return super(Question, self).save(*args, **kwargs)


class Answer(models.Model):
    """Вариант ответа"""
    text = models.TextField()
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='answers')
    correct = models.BooleanField(default=False)
    sequence = models.PositiveIntegerField(verbose_name='Порядок ответа')

    def __str__(self):
        return self.text
    
    def save(self, *args, **kwargs):
        if not self.pk:
            last_sequence = Answer.objects.filter(question=self.question).aggregate(largest=models.Max('sequence'))['largest']
            if last_sequence is not None:
                self.sequence = last_sequence + 1
        return super(Answer, self).save(*args, **kwargs)


class QuizSolution(models.Model):
    """Результат теста юзера"""
    quiz = models.ForeignKey(Quiz, on_delete=models.PROTECT, related_name='solutions')
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='solutions')
    correct_count = models.PositiveIntegerField(default=0)
    wrong_count = models.PositiveIntegerField(default=0)
    point = models.PositiveIntegerField(verbose_name='Оценка за тест', default=0)

    def __str__(self):
        return f'Тест {self.quiz.name} юзера {str(self.user)}'


class QuizSolutionQuestion(models.Model):
    """Ответы юзера на вопросы"""
    quiz_solution = models.ForeignKey(QuizSolution, on_delete=models.CASCADE,
                                      related_name='solution_questions')
    question = models.ForeignKey(Question, on_delete=models.CASCADE,
                                 related_name='solution_questions',
                                 null=True, blank=True)
    given_answer = models.ForeignKey(Answer, on_delete=models.PROTECT,
                                     related_name='solution_questions',
                                     null=True, blank=True)

    def __str__(self):
        return f'{self.quiz_solution}. вопрос: {str(self.question)} ответ: {str(self.given_answer)}'

    def save(self, *args, **kwargs):
        if self.given_answer:
            self.quiz_solution.correct_count = self.quiz_solution.correct_count + 1
        else:
            self.quiz_solution.correct_count = self.quiz_solution.wrong_count + 1
        return super(QuizSolutionQuestion, self).save(*args, **kwargs)
