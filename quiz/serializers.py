from rest_framework import serializers

from quiz.models import Quiz, Question, QuizSolution, QuizSolutionQuestion


class QuizSolutionQuestionSerializer(serializers.ModelSerializer):
    correct = serializers.BooleanField(source='given_answer.correct', read_only=True)

    class Meta:
        model = QuizSolutionQuestion
        fields = ('question', 'given_answer', 'correct')


class QuizSolutionSerializer(serializers.ModelSerializer):
    quiz = serializers.CharField(source='quiz.name')
    quiz_solution_questions = QuizSolutionQuestionSerializer(source='solution_questions', many=True)

    class Meta:
        model = QuizSolution
        fields = ('quiz', 'user', 'correct_count', 'wrong_count', 'point', 'quiz_solution_questions')


class QuestionsAndAnswersSerializer(serializers.ModelSerializer):

    def validate(self, attrs):
        """Checks if the given answer belong to the question"""
        attrs = super(QuestionsAndAnswersSerializer, self).validate(attrs)
        question = Question.objects.filter(pk=attrs['question'].id, answers=attrs['given_answer'])
        if not question.exists():
            raise serializers.ValidationError(f"The given answer({attrs['given_answer']}) "
                                              f"does not belong to this question")
        return attrs

    class Meta:
        model = QuizSolutionQuestion
        fields = ('question', 'given_answer')


class TakeQuizSerializer(serializers.ModelSerializer):
    questions = QuestionsAndAnswersSerializer(source='solution_questions', many=True, required=False)
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault(),
    )

    def validate(self, attrs):
        """Checks if the question belong to the quiz"""
        attrs = super(TakeQuizSerializer, self).validate(attrs)
        solution_questions = attrs.get('solution_questions')
        if solution_questions is not None:
            for solution_question in solution_questions:
                if not Quiz.objects.filter(pk=attrs['quiz'].id, questions=solution_question['question']).exists():
                    raise serializers.ValidationError(f"The given question({solution_question['question']}) "
                                                      f"does not belong to this quiz")
        return attrs

    def create(self, validated_data):
        """Creates with QuizSolutionQuestions together"""
        quiz_solution, created = QuizSolution.objects.get_or_create(quiz=validated_data.pop('quiz'),
                                                                    user_id=validated_data.pop('user').id)
        questions = validated_data.pop('solution_questions', None)
        if questions is not None:
            for question in questions:
                questions, created = QuizSolutionQuestion.objects.get_or_create(quiz_solution=quiz_solution, **question)
                questions.save()
        return quiz_solution

    class Meta:
        model = QuizSolution
        fields = ('user', 'quiz', 'questions')
