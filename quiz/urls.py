from rest_framework.routers import DefaultRouter

from quiz import views

router = DefaultRouter()

router.register('', viewset=views.QuizSolutionViewSet, basename='quiz-solution')
