import json
import pytest

from datetime import datetime

from django.contrib.auth import get_user_model
from django.urls import reverse

from rest_framework.test import APIClient
from faker import Faker
from mixer.backend.django import mixer
from rest_framework_simplejwt.tokens import RefreshToken

from quiz.models import QuizSolution, Quiz

pytestmark = pytest.mark.django_db
fake = Faker()
User = get_user_model()


@pytest.fixture
def api_client():
    user = User.objects.create_user(username='testUser', email='js@js.com', password='js.sj')
    client = APIClient()
    refresh = RefreshToken.for_user(user)
    client.credentials(HTTP_AUTHORIZATION=f'Bearer {refresh.access_token}')

    return client


def test_zero_get_quizzes_should_return_204(api_client):
    url = reverse('quiz:quiz-solution-get-quizzes')
    response = api_client.get(url, {})
    print(response.content)
    assert response.status_code == 204


def test_one_get_quizzes_should_return_200(api_client):
    url = reverse('quiz:quiz-solution-get-quizzes')
    mixer.blend(QuizSolution, user=User.objects.first())
    response = api_client.get(url, {})
    response_content = json.loads(response.content).get('data')[0]
    assert response.status_code == 200
    assert response_content.get('correct_count') == 0
    assert response_content.get('wrong_count') == 0
    assert response_content.get('point') == 0


def test_take_quiz(api_client):
    url = reverse('quiz:quiz-solution-take-quiz')
    deadline = fake.date_time_between(start_date='-1d', end_date='+2d')
    quiz = mixer.blend(Quiz, deadline=deadline)
    quiz_solution = mixer.blend(QuizSolution, user=User.objects.first(), quiz=quiz)
    response = api_client.post(url, {"quiz": quiz.id})
    if quiz_solution.quiz.deadline > datetime.now():
        assert response.status_code == 201
    else:
        assert response.status_code == 403
