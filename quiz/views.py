from django.db.models import Avg
from django.utils import timezone
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from quiz.models import QuizSolution, Quiz
from quiz.serializers import QuizSolutionSerializer, TakeQuizSerializer


class QuizSolutionViewSet(viewsets.ViewSet):

    @action(methods=['GET'], detail=False, url_path='get-quizzes')
    def get_quizzes(self, request, *args, **kwargs):
        """
        Endpoint returns quizzes
        :return: all passed quizzes(QuizSolution's instance) of the authorized userp
        """
        home_works = QuizSolution.objects.filter(user=self.request.user)
        serializer = QuizSolutionSerializer(home_works, many=True)
        if serializer.data:
            data = {
                'data': serializer.data,
                'average_point': home_works.aggregate(avg_point=Avg('point'))['avg_point']
            }
            return Response(data, status=status.HTTP_200_OK)
        return Response({"message": f"This user has not passed quiz yet. ({request.user.username})"},
                        status=status.HTTP_204_NO_CONTENT)

    @action(methods=['POST'], detail=False, url_path='take-quiz')
    def take_quiz(self, request, *args, **kwargs):
        """
        Endpoint saves result of the quiz
        :return: created QuizSolution's instance or Deadline expired message(403)
        """
        serializer = TakeQuizSerializer(data=request.data, context={'request': self.request})
        serializer.is_valid(raise_exception=True)
        deadline = Quiz.objects.get(pk=serializer.validated_data['quiz'].id).deadline
        if deadline > timezone.now():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response({"message": "Deadline expired, You can not take the quiz"},
                            status=status.HTTP_403_FORBIDDEN)
