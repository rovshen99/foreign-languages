import pytest

from datetime import datetime

from django.contrib.auth import get_user_model
from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse

from rest_framework.test import APIClient
from faker import Faker
from mixer.backend.django import mixer
from rest_framework_simplejwt.tokens import RefreshToken

from homework.models import HomeWorkSolution, HomeWork


pytestmark = pytest.mark.django_db
fake = Faker()
User = get_user_model()


@pytest.fixture
def api_client():
    user = User.objects.create_user(username='testUser', email='js@js.com', password='js.sj')
    client = APIClient()
    refresh = RefreshToken.for_user(user)
    client.credentials(HTTP_AUTHORIZATION=f'Bearer {refresh.access_token}')

    return client


def test_zero_get_homework_should_return_204(api_client):
    url = reverse('homework:homework-solution-get-home-works')
    response = api_client.get(url, {})
    assert response.status_code == 204


def test_one_get_homework_should_return_200(api_client):
    url = reverse('homework:homework-solution-get-home-works')
    mixer.blend(HomeWorkSolution, user=User.objects.first())
    response = api_client.get(url, {})
    assert response.status_code == 200


def test_upload_file_homework(api_client):
    deadline = fake.date_time_between(start_date='-1d', end_date='+2d')
    homework = mixer.blend(HomeWork,)
    homework_solution = mixer.blend(HomeWorkSolution, deadline=deadline, homework=homework, user=User.objects.first())
    url = reverse('homework:homework-solution-upload-file', args=(homework_solution.id,))
    file = SimpleUploadedFile("file.txt", b"abc", content_type="text/plain")
    response = api_client.patch(url, {"file": file})
    if homework_solution.deadline > datetime.now():
        assert response.status_code == 200
    else:
        assert response.status_code == 403

