from django.utils import timezone

from homework.models import HomeWorkSolution


def is_homework_deadline_expired(homework_solution: HomeWorkSolution) -> bool:
    """Checking if homework deadline expired, if true homework point = 0"""
    if homework_solution.deadline < timezone.now():
        homework_solution.point = 0
        homework_solution.save()
        return True
    return False
