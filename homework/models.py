import datetime

from django.contrib.auth import get_user_model
from django.db import models
from django.db.models.signals import m2m_changed
from django.dispatch import receiver

from homework.validators import MaxSizeValidator

User = get_user_model()


class HomeWork(models.Model):
    """Домашнее задание"""
    users = models.ManyToManyField(User, related_name='homeworks')
    description = models.TextField()

    def __str__(self):
        return self.description

    def get_users(self):
        return self.users.all()


@receiver(m2m_changed, sender=HomeWork.users.through)
def create_or_update_user_profile(sender, instance, action, **kwargs):
    pk_set = kwargs.pop('pk_set', None)
    if action == 'post_add':
        for user_id in pk_set:
            HomeWorkSolution.objects.create(user_id=user_id, homework=instance).save()


class HomeWorkSolution(models.Model):
    """Решение домашнего задания"""
    file = models.FileField(upload_to='media/files', validators=[MaxSizeValidator(2)], null=True, blank=True)
    point = models.PositiveIntegerField(verbose_name='Оценка за ДЗ', default=0)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='homework_solutions')
    deadline = models.DateTimeField(default=datetime.datetime.now() - datetime.timedelta(days=7))
    done = models.BooleanField(default=False)
    homework = models.ForeignKey(HomeWork, on_delete=models.CASCADE, related_name='homework_solutions')

    def save(self, *args, **kwargs):
        if self.file:
            self.done = True
        return super(HomeWorkSolution, self).save(*args, **kwargs)
