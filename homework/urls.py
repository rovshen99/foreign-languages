from rest_framework.routers import DefaultRouter

from homework import views

router = DefaultRouter()

router.register('', viewset=views.HomeWorkSolutionViewSet, basename='homework-solution')
