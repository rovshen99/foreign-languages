from rest_framework import serializers

from homework.models import HomeWorkSolution


class HomeWorkSolutionSerializer(serializers.ModelSerializer):

    class Meta:
        model = HomeWorkSolution
        fields = '__all__'


class HomeWorkSolutionFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = HomeWorkSolution
        fields = ('id', 'file',)
        read_only_fields = ('id',)

