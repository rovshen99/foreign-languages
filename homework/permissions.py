from rest_framework import permissions


class UserIsOwner(permissions.BasePermission):
    message = "You do not have permission to perform this action, cause it is not authenticated user's instance"

    def has_object_permission(self, request, view, obj):
        return obj.user.id == request.user.id
