from django.contrib import admin

from homework.models import HomeWorkSolution, HomeWork


@admin.register(HomeWorkSolution)
class HomeWorkSolutionAdmin(admin.ModelAdmin):
    list_display = ('id', 'file', 'point', 'user', 'deadline', 'done', 'homework')
    list_display_links = ('user',)


@admin.register(HomeWork)
class HomeWorkAdmin(admin.ModelAdmin):
    list_display = ('get_users', 'description',)

