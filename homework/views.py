from django.db.models import Avg

from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from homework.models import HomeWorkSolution
from homework.permissions import UserIsOwner
from homework.serializers import HomeWorkSolutionSerializer, HomeWorkSolutionFileSerializer
from homework.services import is_homework_deadline_expired


class HomeWorkSolutionViewSet(viewsets.ModelViewSet):
    queryset = HomeWorkSolution.objects.all()
    serializer_class = HomeWorkSolutionSerializer

    @action(methods=['GET'], detail=False, url_path='get-homeworks')
    def get_home_works(self, request, *args, **kwargs):
        """
        Endpoint returns all home_works
        :param request:
        :param args:
        :param kwargs:
        :return: all home_works(HomeWorkSolution) of the authorized user
        """
        done = int(self.request.query_params.get('done', 0))
        if done:
            home_works = HomeWorkSolution.objects.filter(user=self.request.user, done=done)
        else:
            home_works = HomeWorkSolution.objects.filter(user=self.request.user)
        serializer = self.serializer_class(home_works, many=True)
        if serializer.data:
            data = {
                'data': serializer.data,
                'average_point': home_works.aggregate(avg_point=Avg('point'))['avg_point']
            }
            return Response(data, status=status.HTTP_200_OK)
        return Response({"message": f"No Homework for the authorized user({request.user.username})"},
                        status=status.HTTP_204_NO_CONTENT)

    @action(methods=['PATCH'], detail=True, permission_classes=[UserIsOwner])
    def upload_file(self, request, pk=None):
        """Upload a file to homework solution"""
        homework_solution = self.get_object()
        if is_homework_deadline_expired(homework_solution):
            return Response({"message": "Deadline expired, You can not upload file. You got 0"},
                            status=status.HTTP_403_FORBIDDEN)
        serializer = HomeWorkSolutionFileSerializer(homework_solution, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)
