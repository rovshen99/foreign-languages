from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from homework.urls import router as homework_router
from quiz.urls import router as quiz_router

urlpatterns = [
    path('admin/', admin.site.urls),
    path('_nested_admin/', include('nested_admin.urls')),
    path('api/homework/', include((homework_router.urls, 'homework'),  namespace='homework')),
    path('api/quiz/', include((quiz_router.urls, 'quiz'), namespace='quiz')),
    path('api/user/', include(('user.urls', 'user'), namespace='user'),),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
