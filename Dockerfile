FROM python:3.8

ENV PYTHONDONTWRITTENBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /usr/src/foreign_languages

COPY . /usr/src/foreign_languages

RUN apt-get update && apt-get install -y git

RUN pip3 install --upgrade pip
RUN python -m venv /venv

RUN pip3 install -r requirements.txt

COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["sh", "/entrypoint.sh"]