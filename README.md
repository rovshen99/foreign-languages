# Foreign Languages

**Clone from Bitbucket and create .env file**

*And now you just have to run docker-compose*

    docker-compose up --build

*If you want to create super user you can use 1 of 2 commands*

    docker-compose run web sh -c "python manage.py createsuperuser"
or

    docker exec -it <dockercontainerid> python manage.py createsuperuser
