import pytest

from django.contrib.auth import get_user_model
from django.urls import reverse

pytestmark = pytest.mark.django_db
User = get_user_model()


def test_user_register(client) -> None:
    url = reverse('user:register')
    response = client.post(url, {'username': 'testUser', 'password': '123456'},)
    assert response.status_code == 201
