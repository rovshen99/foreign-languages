from django.urls import path

from rest_framework_simplejwt import views as jwt_views

from user import views


urlpatterns = [
    path('token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/verify/', jwt_views.TokenVerifyView.as_view(), name='token_verify_pair'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('register/', views.CreateUserView.as_view(), name='register')
]